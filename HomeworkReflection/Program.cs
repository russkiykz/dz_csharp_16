﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace HomeworkReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> typeDictionary = new Dictionary<string, string>();
            typeDictionary.Add("String", "nvarchar");
            typeDictionary.Add("Double", "float");
            typeDictionary.Add("DateTime", "date");
            typeDictionary.Add("Int32", "int");
            Console.WriteLine("Введите путь до сборки с названием и расширением типа (dll): ");
            Assembly assemblyForAnalysis = Assembly.LoadFile(Console.ReadLine());
            foreach(var type in assemblyForAnalysis.GetTypes())
            {
                if (type.IsClass && !type.IsAbstract)
                {
                    Console.WriteLine($"create table {type.Name}s\n(");
                    foreach (var property in type.GetProperties())
                    {
                        if (typeDictionary.ContainsKey(property.PropertyType.Name))
                        {
                            Console.Write($"{property.Name} ");
                            switch (property.PropertyType.Name)
                            {
                                case "Int32":
                                    Console.Write($"{typeDictionary[property.PropertyType.Name]}");
                                    if (property.Name.ToLower() == "id")
                                    {
                                        Console.Write(" primary key identity");
                                    }
                                    Console.Write(" not null");
                                    break;
                                case "String":
                                    Console.Write($"{typeDictionary[property.PropertyType.Name]}(max)");
                                    Console.Write(" null");
                                    break;
                                case "Double":
                                    Console.Write($"{typeDictionary[property.PropertyType.Name]}");
                                    Console.Write(" not null");
                                    break;
                                case "DateTime":
                                    Console.Write($"{typeDictionary[property.PropertyType.Name]}");
                                    break;
                            }
                            Console.WriteLine();
                        }
                    }
                    Console.WriteLine(")");
                }
                Console.WriteLine();
            }
        }
    }
}
